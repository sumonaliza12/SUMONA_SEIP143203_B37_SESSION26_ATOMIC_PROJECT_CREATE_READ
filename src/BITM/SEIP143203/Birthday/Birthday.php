<?php
namespace App\Birthday;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Birthday extends DB
{

    public $name="";
    public $date="";

    public function __construct()
    {
        parent:: __construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function setData($data=NULL)
    {

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }

        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
    }


    public function store()
    {

        $arrData = array($this->name, $this->date);

        $sql = "INSERT INTO `birthday` (`name`, `date`) VALUES (?,?);";

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthdate: $this->date ] <br> Data Has Been Inserted Successfully!</h3></div>");
        } else {
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthdate: $this->date ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }



        Utility::redirect('create.php');

    }

    public function index()
    {

    }







    /*

        public function store(){

              Message::message("<h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3>");
              Utility::redirect('create.php');

        }
    */





} //end of class

?>

