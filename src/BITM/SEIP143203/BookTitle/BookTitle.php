<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class BookTitle extends DB
{

    public $book_name="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function setData($data=NULL)
    {


        if(array_key_exists('book_name',$data)){
            $this->book_name = $data['book_name'];
        }

        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }


    public function store(){

        $arrData  = array($this->book_name,$this->author_name);

        $sql = "INSERT INTO book_title ( book_name, author_name) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("<div id='msg'></div><h3 align='center'>[ Book Title: $this->book_name ] , [ Book Title: $this->book_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        } else {
            Message::message("<div id='msg'></div><h3 align='center'>[ Author Name: $this->author_name ] , [ Author Name: $this->author_name ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }


        Utility::redirect('create.php');

    }

    public function index($Mode="ASSOC")
    {
        $mode=strtoupper($Mode);
        $sql="SELECT * FROM book_title";
        $STH=$this->DBH->query($sql);

        if($Mode=="OBJ")

            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData=$STH->fetchAll();

        return $arrAllData;
    }







    /*

        public function store(){

              Message::message("<h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3>");
              Utility::redirect('create.php');

        }
    */





}

?>

