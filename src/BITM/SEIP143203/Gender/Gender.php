<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 11/10/2016
 * Time: 12:32 AM
 */

namespace App\Gender;


use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Gender extends DB{

    public $name="";
    public $gender="";

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }
    public function index()
    {
        echo " I am inside of index method";
    }

    public function setData($data = NULL)
    {

        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('gender',$data))
        {
            $this->gender = $data['gender'];
        }
    }
    public function  store()
    {

        $query = $this->DBH-> prepare("INSERT INTO gender(name, gender)
        VALUES(:name,:gender)");
        $query->execute(array(
            "name" => $this->name,
            "gender" => $this->gender,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->name ] , [ GenderValue: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->name ] , [ GenderValue: $this->gender ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}