<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 11/10/2016
 * Time: 8:43 AM
 */

namespace App\Hobbies;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;



class Hobbies extends DB{

    public $name="";
    public $hobbies="";

    public function __construct(){
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {

        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('hobbies',$data))
        {
            $this->hobbies = implode(',', $_POST['hobbies']);

        }
    }
    public function  store()
    {

        $query = $this->DBH-> prepare("INSERT INTO hobbies(name, hobbies)
        VALUES(:name,:hobbies)");
        $query->execute(array(
            "name" => $this->namee,
            "hobbies" => $this->hobbies,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}