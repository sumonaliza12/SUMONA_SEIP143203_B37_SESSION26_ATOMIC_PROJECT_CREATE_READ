<?php
namespace App\Email;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Email extends DB{

    public $name="";
    public $mail_address="";

    public function __construct(){
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {

        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('mail_address',$data))
        {
            $this->mail_address = $data['mail_address'];
        }
    }
    public function  store()
    {

        $query = $this->DBH-> prepare("INSERT INTO email(name, mail_address)
        VALUES(:name,:mail_address)");
        $query->execute(array(
            "name" => $this->name,
            "mail_address" => $this->mail_address,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Email: $this->mail_address ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->name ] , [ Email: $this->mail_address ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}