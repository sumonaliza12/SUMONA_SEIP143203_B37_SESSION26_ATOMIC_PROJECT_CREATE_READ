<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Summary Of Organization FORM</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/css/form-elements.css">
    <link rel="stylesheet" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/ico/apple-touch-icon-57-precomposed.png">



</head>

<body>

<!-- Top menu -->
<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">Bootstrap Registration Form Template</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
							<span class="li-text">
								Put some text or
							</span>
                    <a href="#"><strong>links</strong></a>
                    <span class="li-text">
								here, or some icons:
							</span>
                    <span class="li-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-envelope"></i></a>
								<a href="#"><i class="fa fa-skype"></i></a>
							</span>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 text">
                    <h1><strong>ATOMIC PROJECT: </strong> SUMMARY OF ORGANIZATION</h1>
                    <div class="description">

                    </div>
                    <div class="top-big-link">
                        <img src="">

                    </div>
                </div>
                <div class="col-sm-5 form-box">
                    <div class="form-top">
                        <div class="form-top-left">

                            <p>Fill in the form below to get instant access:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-address-card"></i>
                        </div>
                    </div>
                    <div class="form-bottom">

                        <?php
                        echo '
                       
                        <form role="form" action="store.php" method="post" class="registration-form">
                            <div class="form-group">
                                <label class="sr-only" for="form-name"> NAME</label>
                                <input type="text" name="name" placeholder="Enter your name..." class="form-book_name form-control" id="form-name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-date">ID</label>
                                <input type="number" name="id" placeholder="Enter your ID..." class="form-author_name form-control" id="form-author_name" >
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-date">DETAILS</label>
                                <input type="text" name="details" placeholder="Enter your details..." class="form-author_name form-control" id="form-author_name" >
                            </div>


                            <button type="submit" class="btn">Click Here To Insert!</button>
                        </form>';
                     ?>
                    </div>
                    <div class="footer">
                        <ul class="pagination">
                            <li class="disabled"><a href="#">«</a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    function removeFadeOut( el, speed ) {
        var seconds = speed/1000;
        el.style.transition = "opacity "+seconds+"s ease";

        el.style.opacity = 0;
        setTimeout(function() {
            el.parentNode.removeChild(el);
        }, speed);
    }

    removeFadeOut(document.getElementById('msg'), 9000);
</script>


<!-- Javascript -->
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/js/jquery-1.11.1.min.js"></script>
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/js/jquery.backstretch.min.js"></script>
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/js/retina-1.1.0.min.js"></script>
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../src/BITM/SEIP143203/SummaryOfOrganization/resources/js/placeholder.js"></script>
<![endif]-->

</body>

</html>